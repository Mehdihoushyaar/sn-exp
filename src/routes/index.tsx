import { useRoutes } from "react-router-dom";
import ProductsPage from "../pages/ProductsPage";
import { ROUTES } from "./routesList";

export const AppRoutes = () => {
  
  const iterateRoutes = ROUTES.map(({ path, Element }) => {
    return { path, element: <Element /> };
  });

  const routes = useRoutes([
    ...iterateRoutes,
    { path: "/", element: <ProductsPage /> },
  ]);

  return routes;
};
