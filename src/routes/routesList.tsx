import ProductsPage from "../pages/ProductsPage";

export const ROUTES = [
  {
    path: "/products",
    Element: ProductsPage,
  },
  {
    path: "/",
    Element: ProductsPage,
  },
];
