import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.response.use(
  (response) => {
    const { data, status } = response;
    if (status !== 200) {
      alert("There is a problem");
    }
    return data;
  },
  (exp) => {
    return Promise.reject(exp);
  }
);

export default instance;
