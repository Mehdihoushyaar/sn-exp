export default class StorageService {
  static get = (key: string): string | null => {
    return localStorage.getItem(key);
  };

  static set = (key: string, value = ""): void => {
    localStorage.setItem(key, JSON.stringify(value));
  };

  static remove = (key: string): void => {
    localStorage.removeItem(key);
  };
}
