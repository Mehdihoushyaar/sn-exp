import axiosIns from "../services/axiosService";

const fetchData = async (url: string): Promise<any> => {
  const response = await axiosIns({
    url,
    method: "GET",
  });
  return response;
};

export { fetchData };
