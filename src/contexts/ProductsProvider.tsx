import React, {
  createContext, ReactNode, useContext, useEffect, useState
} from "react";
import { ENDPOINTS } from "../constants/api";
import axiosInstance from "../services/axiosService";
import {
  CategoryType,
  FetchProductsResponseType,
  IProductsContextProps,
  ProductType
} from "./types";

export const ProductsContext = createContext<IProductsContextProps>({
  loading: false,
  products: [],
  categories: [],
  sort: "product_price_asc",
  menuCategoryId: undefined,
  page: 0,
  total: 0,
  selectedCategoryId: undefined,
  selectedFilterId: undefined,
  setSort: () => {},
  setMenuCategoryId: () => {},
  setPage: () => {},
  setSelectedCategoryId: () => {},
  setSelectedFilterId: () => {},
});

type Props = {
  children: ReactNode;
};

const ProductsContextProvider: React.FC<Props> = ({ children }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [products, setProducts] = useState<ProductType[]>([]);
  const [categories, setCategories] = useState<CategoryType[]>([]);
  const [sort, setSort] = useState<string>("product_price_asc");
  const [menuCategoryId, setMenuCategoryId] = useState<number | undefined>();
  const [vendorCode, setVendorCode] = useState<string | undefined>("po9qzk");
  const [page, setPage] = useState<number>(1);
  const [total, setTotal] = useState<number>(0);
  const [selectedCategoryId, setSelectedCategoryId] = useState<
    number | undefined
  >();
  const [selectedFilterId, setSelectedFilterId] = useState<
    number | undefined
  >();

  useEffect(() => {
    fetchProducts();
  }, [sort, vendorCode, page, selectedCategoryId, selectedFilterId, menuCategoryId]);

  const fetchProducts = async () => {
    setLoading(true);
    let url = ENDPOINTS.PRODUCT_VARIATION;
    if (sort) url += `&sort=${sort}`;
    if (menuCategoryId) url += `&menu_category_id=${menuCategoryId}`;
    if (vendorCode) url += `&vendorCode=${vendorCode}`;
    if (page) url += `&page=${page}`;
    if (selectedCategoryId)
      url += `&selected_category_id=${selectedCategoryId}`;
    if (selectedFilterId) url += `&selected_filter_id=${selectedFilterId}`;

    try {
      const {
        data: { code, product_variations, categories, meta },
      } = await axiosInstance.get<FetchProductsResponseType>(url);

      setProducts(product_variations);
      setCategories(categories);
      setTotal(meta?.pagination?.total)
    } catch (exp) {
      console.log(exp);
    } finally {
      setLoading(false);
    }
  };

  return (
    <ProductsContext.Provider
      value={{
        loading,
        products,
        categories,
        sort,
        menuCategoryId,
        page,
        total,
        selectedCategoryId,
        selectedFilterId,
        setSort,
        setMenuCategoryId,
        setPage,
        setSelectedCategoryId,
        setSelectedFilterId,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

const useProductsContext = () => useContext(ProductsContext);

const useProductsContextActions = () => {
  const {
    setSort,
    setMenuCategoryId,
    setPage,
    setSelectedCategoryId,
    setSelectedFilterId,
  } = useProductsContext();
  const setSortAction = (data: string) => setSort(data);
  const setMenuCategoryIdAction = (data: number) => setMenuCategoryId(data);
  const setPageAction = (data: number) => setPage(data);
  const setSelectedCategoryIdAction = (data: number) =>
    setSelectedCategoryId(data);
  const setSelectedFilterIdAction = (data: number) => setSelectedFilterId(data);

  return {
    setSortAction,
    setMenuCategoryIdAction,
    setPageAction,
    setSelectedCategoryIdAction,
    setSelectedFilterIdAction,
  };
};

export {
  useProductsContext,
  useProductsContextActions,
  ProductsContextProvider,
};
