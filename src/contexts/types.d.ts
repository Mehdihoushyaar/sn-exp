export interface ProductImageType {
  imageThumbnailSrc: string;
  imageSrc: string;
  imageUserType: string;
  isDefault: boolean;
}

export interface ProductType {
  id: number;
  title: string;
  productVariationTitle: string;
  score: number;
  barcode: string;
  rating: number;
  type: string;
  discount: number;
  description: string;
  price: number;
  productTitle: string;
  productId: string | number;
  brandId: number;
  brandTitle: string;
  brandLogo: string;
  brandDescription?: string;
  vat: number;
  disabledUntil: boolean;
  containerPrice: number;
  featured: string;
  images: ProductImageType[];
  highlight: string;
  popularityBadgeName: string;
  productToppings: any;
  popularityBadgeURL: string;
  capacity: number;
  active: boolean;
  discountRatio: number;
}

export interface CategoryType {
  id: number;
  title: string;
  lft: number;
  rgt: number;
  image: string;
}

export interface IProductsContextProps {
  loading: boolean;
  products: ProductType[];
  categories: CategoryType[];
  sort: string;
  menuCategoryId?: number;
  page: number;
  total: number;
  selectedCategoryId?: number;
  selectedFilterId?: number;
  setSort: Dispatch<SetStateAction<string>>;
  setMenuCategoryId: Dispatch<SetStateAction<number>>;
  setPage: Dispatch<SetStateAction<number>>;
  setSelectedCategoryId: Dispatch<SetStateAction<number>>;
  setSelectedFilterId: Dispatch<SetStateAction<number>>;
}

export interface FetchProductsResponseType {
  product_variations: ProductType[];
  categories: CategoryType[];
  meta: any;
  status: boolean;
  code: number;
}
