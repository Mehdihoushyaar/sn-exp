import { useEffect, useState } from "react";
import { fetchData } from "../helpers/endpoints";

const useFetch = (url: string) => {
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    (async () => {
      const res = await fetchData(url);
      setData(res);
    })();
  }, [url]);

  return data;
};

export { useFetch };
