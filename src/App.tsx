import React from "react";
import "./App.css";
import { AppRoutes } from "./routes";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useRoutes,
} from "react-router-dom";
import ProductsPage from "./pages/ProductsPage";
import { ProductsContextProvider } from "./contexts";

const Appp = () => {
  let routes = useRoutes([{ path: "/", element: <ProductsPage /> }]);
  return routes;
};

const App = () => {
  return (
    <div className="App">
      <ProductsContextProvider>
        <Router>
          <AppRoutes />
        </Router>
      </ProductsContextProvider>
    </div>
  );
};

export default App;
