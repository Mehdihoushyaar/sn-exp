import { FC } from "react";
import { SSelectType } from "../types";
import styles from "./SSelect.module.scss";

const SSelect: FC<SSelectType> = ({ options, value, onChange, label }) => {
  return (
    <div className={styles.selectInput}>
      <span className={styles.label}>{label}</span>
      <select onChange={onChange}>
        {options.map(({ value, label }) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SSelect;
