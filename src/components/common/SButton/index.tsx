import React from "react";
import { Link } from "react-router-dom";
import { SButtonType } from "../types";
import styles from "./SButton.module.scss";

export const SButton: React.FC<SButtonType> = ({
  theme = "outlined",
  additionalClassNames = "",
  additionalStyles = {},
  href = "",
  to = "",
  targetBlank = false,
  onClick = () => {},
  title,
  fullWidth = false,
  disabled = false,
  ...rest
}) => {
  const renderChildren = () => {
    if (href) {
      return (
        <a href={href} target={targetBlank ? "_blank" : "_self"}>
          {title}
        </a>
      );
    } else if (to) {
      return <Link to={to}>{title}</Link>;
    }
    return <span>{title}</span>;
  };

  return (
    <button
      className={`
        ${styles.button} 
        ${styles[theme]} 
        ${disabled ? styles.disabled : ""} 
        ${fullWidth ? styles.fullWidth : ""} 
        ${additionalClassNames}`}
      style={additionalStyles}
      onClick={onClick}
      disabled={disabled}
      {...rest}
    >
      {renderChildren()}
    </button>
  );
};
