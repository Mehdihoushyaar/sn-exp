import React, { FC } from "react";
import { ProductType } from "../../../contexts/types";
import { SButton } from "../SButton";
import styles from "./ProductCard.module.scss";

const ProductCard: FC<ProductType> = ({ title, images, price }) => {
  return (
    <div className={styles.productCard}>
      <div className={styles.image}>
        <img src={images[0]?.imageSrc} alt={title} />
      </div>
      <div className={styles.content}>
        <div className={styles.title}>{title}</div>
        <div className={styles.footer}>
          <div className={styles.price}>
            <div className={styles.price_umber}>{price}</div>
            <div className={styles.price_unit}>تومان</div>
          </div>
          <SButton title={"افزودن"} />
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
