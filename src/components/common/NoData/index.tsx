import { FC } from "react";
import styles from './NoData.module.scss'

const NoData: FC<{ message?: string }> = ({ message = "محصولی یافت نشد" }) => {
  return (
    <div className={styles.noData}>
      <span>{message}</span>
    </div>
  );
};

export default NoData;
