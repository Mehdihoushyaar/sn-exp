import { ButtonHTMLAttributes } from "react";

export interface SButtonType {
  title: any;
  theme?: string;
  href?: string;
  to?: string;
  additionalClassNames?: string;
  additionalStyles?: {};
  fullWidth?: boolean;
  disabled?: boolean;
  targetBlank?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

export interface SSelectType {
  options: { value: any; label: string }[];
  onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  value?: string | number;
  label?: string;
}
[];
