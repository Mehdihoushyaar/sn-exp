import styles from "./ProductCardLoader.module.scss";

const ProductCardLoader = () => {
  return (
    <div className={styles.loader}>
      <div className={styles.image}></div>
      <div className={styles.title}></div>
      <div className={styles.footer}></div>
    </div>
  );
};

export default ProductCardLoader;
