import { FC } from "react";
import Pagination from "rc-pagination";
import "./SPagination.css";

const SPagination: FC<any> = ({ current, onChange, total }) => {
  return (
    <Pagination
      total={total}
      pageSize={20}
      hideOnSinglePage
      current={current}
      onChange={onChange}
    />
  );
};

export default SPagination;
