import { FC } from "react";
import { SButton } from "../../../../components/common";
import SSelect from "../../../../components/common/SSelect";
import {
  useProductsContext,
  useProductsContextActions,
} from "../../../../contexts";
import styles from "./Filters.module.scss";
import FilterIcon from "../../../../assets/images/filter.png";

const Filters: FC = () => {
  const { setSortAction } = useProductsContextActions();
  const { sort } = useProductsContext();

  const renderSort = () => {
    return [
      { label: "ارزانترین", value: "product_price_asc" },
      { label: "گرانترین", value: "product_price_desc" },
      { label: "جدیدترین", value: "product_newest" },
      { label: "پرفروش ترین", value: "best_selling" },
    ];
  };

  return (
    <div className={styles.filters}>
      <div className={styles.sort_advancedBtn}>
        <div className={styles.sort}>
        <SSelect
          options={renderSort()}
          onChange={(e) => setSortAction(e.target.value)}
          label="مرتب سازی بر اساس:"
        />
        </div>
        <div className={styles.advancedBtn}>
          <SButton title={'جستجوی پیشرفته'} theme='filled' />
        </div>
      </div>
    </div>
  );
};

export default Filters;
