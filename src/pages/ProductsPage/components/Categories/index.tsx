import { FC } from "react";
import {
  useProductsContext,
  useProductsContextActions,
} from "../../../../contexts";
import styles from "./Categories.module.scss";

const Categories: FC = () => {
  const { setSelectedCategoryIdAction } = useProductsContextActions();
  const { categories, selectedCategoryId } = useProductsContext();

const handleClick = (id:number) => {
  setSelectedCategoryIdAction(id)
}

  return (
    <div className={styles.categories}>
      {categories?.map(({ id, title, image }) => (
        <div key={id} className={styles.categoryWrapper}>
          <div className={`${styles.categoryCard} ${id == selectedCategoryId ? styles.active : ''}`} onClick={() => handleClick(id)}>
            <div className={styles.image}>
              <img src={image} alt={title} />
            </div>
            <div className={styles.title}>{title}</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Categories;
