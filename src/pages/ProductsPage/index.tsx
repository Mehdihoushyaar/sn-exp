import React, { useEffect } from "react";
import Layout from "../../components/elements";
import { useProductsContext, useProductsContextActions } from "../../contexts";
import StorageService from "../../services/storageService";
import { SButton } from "../../components/common";
import ProductCard from "../../components/common/ProductCard";
import styles from "./ProductsPage.module.scss";
import ProductCardLoader from "../../components/common/ProductCardLoader";
import SPagination from "../../components/common/SPagination";
import NoData from "../../components/common/NoData";
import Filters from "./components/Filters";
import Categories from "./components/Categories";


const ProductsPage = () => {
  const { setPageAction } = useProductsContextActions();
  const { loading, products , page, total} = useProductsContext();

  const renderLoaders = () => {
    return (
      <div className={styles.loadersList}>
        {[...new Array(6)].map((i) => (
          <div className={styles.loaderWrapper}>
            <ProductCardLoader />
           </div>
        ))}
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <Layout>
        <Categories />
        <Filters />
        <span className={styles.sectionTitle}>لیست محصولات</span>
        <div className={styles.productsList}>
          {!loading
            ? products.length ? products.map((item) => (
                <div key={item.id} className={styles.productCardWrapper}>
                  <ProductCard {...item} />
                </div>
              )): <NoData />
            : renderLoaders()}
        </div>
        <SPagination total={total} onChange={setPageAction} current={page}/>
      </Layout>
    </div>
  );
};

export default ProductsPage;
